import { TestBed } from '@angular/core/testing';

import { SchoolserviceService } from './schoolservice.service';

describe('SchoolserviceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SchoolserviceService = TestBed.get(SchoolserviceService);
    expect(service).toBeTruthy();
  });
});
