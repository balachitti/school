import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SchoolserviceService } from './schoolservice.service';

import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { SchoolregisterComponent } from './schoolregister/schoolregister.component';
import { AddStudentDataComponent } from './schoolregister/add-student-data/add-student-data.component';
import { EditStudentDataComponent } from './schoolregister/edit-student-data/edit-student-data.component';
import { DisplayStudentDataComponent } from './schoolregister/display-student-data/display-student-data.component';




@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    SchoolregisterComponent,
    DisplayStudentDataComponent,
    AddStudentDataComponent,
    EditStudentDataComponent,
    DisplayStudentDataComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [SchoolserviceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
