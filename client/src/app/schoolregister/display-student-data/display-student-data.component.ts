import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SchoolserviceService } from 'src/app/schoolservice.service';
import { async } from '@angular/core/testing';

@Component({
  selector: 'app-display-student-data',
  templateUrl: './display-student-data.component.html',
  styleUrls: ['./display-student-data.component.css']
})
export class DisplayStudentDataComponent implements OnInit {
  Student: any = [];
  schoolname: string;
  ispublic = false;

  constructor(
    private actRoute: ActivatedRoute,
    private router: Router,
    private schoolservice: SchoolserviceService
  ) {
    this.schoolname = this.actRoute.snapshot.paramMap.get(`schoolname`);
    if (this.schoolname == 'public') {
      console.log('hi');
      this.ispublic = true
      this.readbackupstudent();

    } else {
      this.readstudent();
    }
  }

  ngOnInit() {
    // window.alert(`if new school click Add register button`);
  }
  readstudent() {
    this.schoolservice.viewStudentsData(this.schoolname).subscribe(data => {
      this.Student = data;
      console.log(this.Student);
    });
  }
  readbackupstudent() {
    this.schoolservice.viewBackupData(this.schoolname).subscribe(data => {
      this.Student = data;
      console.log(this.Student);
    });
  }
  removeStudent(student, index) {
    if (window.confirm('Are you sure?')) {
      this.schoolservice
        .backupStudentdata(this.schoolname, student.student_id)
        .subscribe(res => {
          console.log(res);
        });
      this.schoolservice
        .deleteStudentData(this.schoolname, student.student_id)
        .subscribe(data => {
          this.Student.splice(index, 1);
        });
    }

  }
}
