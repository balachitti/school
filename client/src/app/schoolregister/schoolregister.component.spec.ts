import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SchoolregisterComponent } from './schoolregister.component';

describe('SchoolregisterComponent', () => {
  let component: SchoolregisterComponent;
  let fixture: ComponentFixture<SchoolregisterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SchoolregisterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SchoolregisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
