import { Component, OnInit } from '@angular/core';
import { Student } from 'src/app/student';
import { SchoolserviceService } from 'src/app/schoolservice.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';



@Component({
  selector: 'app-edit-student-data',
  templateUrl: './edit-student-data.component.html',
  styleUrls: ['./edit-student-data.component.css']
})
export class EditStudentDataComponent implements OnInit {
  schoolname: string;
  id;
  public submitted = false;
  studentdata: Student[];
  editForm: FormGroup;
  serverErrorMessages: string;
  languages: any = ['tamil', 'english', 'french', 'sanskrit', 'telugu', 'german', 'spanish', 'hindi', 'malayalam'];
  constructor(
    private actRoute: ActivatedRoute,
    public fb: FormBuilder,
    private router: Router,
    private schoolservice: SchoolserviceService
  ) {
    this.schoolname = this.actRoute.snapshot.paramMap.get(`schoolname`);
    this.id = this.actRoute.snapshot.paramMap.get('id');
       }

  ngOnInit() {
    this.updateStudent();
    this.getStudent(this.schoolname, this.id);
    this.editForm = this.fb.group({
      student_id : ['', [Validators.required,Validators.pattern(/[0-9]+$/)]],
      student_name : ['', [Validators.required, Validators.pattern(/([A-Za-z]+$)/)]],
      location : ['', [Validators.required]],
      languages : ['', [Validators.required]]
    });
  }
  updateProfile(e) {
    this.editForm.get('languages').setValue(e, {
      onlySelf: true
    });
  }
  get myForm() {
    return this.editForm.controls;
  }
  getStudent(schoolname, id) {
    this.schoolservice.viewStudent(schoolname, id).subscribe(data => {
      console.log(data);
      this.editForm.setValue({
        student_id: data[0].student_id,
        student_name: data[0].student_name,
        location: data[0].location,
        languages: data[0].languages
        });
    });

  }

  updateStudent() {
    this.editForm = this.fb.group({
      student_id : ['', [Validators.required]],
      student_name : ['', [Validators.required, Validators.pattern(/([A-Za-z]+$)/)]],
      location : ['', [Validators.required]],
      languages : ['', [Validators.required]]
    });
  }
  onSubmit() {
    this.submitted = true;
    // console.log(this.submitted)
    if (!this.editForm.valid) {
      return false;
    } else {

      // this.router.navigateByUrl('/employees-list');

      console.log(this.editForm.value);
      this.schoolservice.updateStudentData(this.schoolname, this.id, this.editForm.value).subscribe(
        (res) => {
          console.log(res);
          this.router.navigateByUrl(`/displaystudentdata/${this.schoolname}`);
        }
        );
  }
}

}
