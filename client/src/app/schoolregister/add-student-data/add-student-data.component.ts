import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { SchoolserviceService } from 'src/app/schoolservice.service';

@Component({
  selector: 'app-add-student-data',
  templateUrl: './add-student-data.component.html',
  styleUrls: ['./add-student-data.component.css']
})
export class AddStudentDataComponent implements OnInit {
  schoolname: string;
  public submitted = false;
  studentForm: FormGroup;
  serverErrorMessages: string;
  languages: any = [
    'tamil',
    'english',
    'french',
    'sanskrit',
    'telugu',
    'german',
    'spanish',
    'hindi',
    'malayalam'
  ];
  constructor(
    private actRoute: ActivatedRoute,
    public fb: FormBuilder,
    private router: Router,
    private schoolservice: SchoolserviceService
  ) {
    this.schoolname = this.actRoute.snapshot.paramMap.get(`schoolname`);
    this.mainForm();
  }

  ngOnInit() {}
  mainForm() {
    this.studentForm = this.fb.group({
      // student_id : ['', [Validators.required, Validators.pattern(/(ACE[0-9]{4})$/)]],
      student_name: [
        '',
        [Validators.required, Validators.pattern(/([A-Za-z]+$)/)]
      ],
      location: ['', [Validators.required]],
      languages: ['', [Validators.required]]
    });
  }
  updateProfile(e) {
    this.studentForm.get('languages').setValue(e, {
      onlySelf: true
    });
  }
  get myForm() {
    return this.studentForm.controls;
  }
  onSubmit() {
    this.submitted = true;
    // console.log(this.submitted)
    if (!this.studentForm.valid) {
      return false;
    } else {
      // this.router.navigateByUrl('/');

      console.log(this.studentForm.value);
      this.schoolservice
        .addStudent(this.schoolname, this.studentForm.value)
        .subscribe(res => {
          console.log(res);
          this.router.navigateByUrl(`/displaystudentdata/${this.schoolname}`);
          });



    }
  }
}
