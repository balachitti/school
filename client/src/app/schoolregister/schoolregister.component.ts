import { Component, OnInit  } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SchoolserviceService } from '../schoolservice.service';

@Component({
  selector: 'app-schoolregister',
  templateUrl: './schoolregister.component.html',
  styleUrls: ['./schoolregister.component.css']
})
export class SchoolregisterComponent implements OnInit {
  schoolname: string;
  ispublic = false;
  isschool = true;
  constructor(
    private actRoute: ActivatedRoute,
    private router: Router,
    private schoolservices: SchoolserviceService
  ) {
    this.schoolname = this.actRoute.snapshot.paramMap.get(`schoolname`);


    // tslint:disable-next-line:triple-

    this.ispuplic();

  }

  ngOnInit() {
    console.log(this.schoolname);
    //this.router.navigate([`/displaystudentdata/${this.schoolname}`]);
  }
  createregisiter() {
    this.schoolservices.createRegister(this.schoolname).subscribe(Response => {
      console.log(Response);
    });
  }
 ispuplic() {
  if (this.schoolname == 'public') {
    console.log('hi');
    this.ispublic = true;
    this.isschool = false;
    this.router.navigate([`/displaystudentdata/${this.schoolname}`]);
    } else {
      //this.router.navigate([`/displaystudentdata/${this.schoolname}`]);
    }
 }
}
