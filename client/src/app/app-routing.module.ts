import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SchoolregisterComponent } from './schoolregister/schoolregister.component';
import { AddStudentDataComponent } from './schoolregister/add-student-data/add-student-data.component';
import { DisplayStudentDataComponent } from './schoolregister/display-student-data/display-student-data.component';
import { EditStudentDataComponent } from './schoolregister/edit-student-data/edit-student-data.component';


const routes: Routes = [
 // { path: '', pathMatch: 'full', redirectTo: 'Dashboard' },
  {path: 'Dashboard', component: DashboardComponent},
  {path: 'Register/:schoolname', component: SchoolregisterComponent},
  {path: 'addstudent/:schoolname', component: AddStudentDataComponent},
  {path: 'displaystudentdata/:schoolname', component: DisplayStudentDataComponent},
  {path: 'editstudentdata/:schoolname/:id', component: EditStudentDataComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
