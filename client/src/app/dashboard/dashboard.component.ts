import { Component, OnInit } from '@angular/core';
import { SchoolserviceService } from '../schoolservice.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  Schools: any = [];
  addbuffer = false;
  listbuffer = true;
  constructor(
    private schoolservice: SchoolserviceService,
    private router: Router
  ) {
    this.readschool();
  }

  ngOnInit() {}
  readschool() {
    this.schoolservice.getSchool().subscribe(data => {
      this.Schools = data;
      console.log(data);
      console.log(this.Schools);
    });
  }
  // onsubmit(schoolname) {
  //   console.log(school.value)

  // }
  onsubmit(schoolname: string) {
    console.log(schoolname);
    this.schoolservice.createSchool(schoolname).subscribe(Response => {
      console.log(Response);
      this.router.navigateByUrl(`/`);
    });
  }
   removeSchool(school, index) {

    this.schoolservice
      .backupSchooldata(school.schoolname)
      .subscribe(Response => {
        console.log(Response);
      });

    setTimeout(() => {
      this.deleteSchool(school, index);
    }, 3000);
    // console.log(school.schoolname);
    // this.schoolservice.dropSchool(school.schoolname).subscribe(Response => {
    //   console.log(Response);
    //   this.Schools.splice(index, 1);
    // });
  }
  addschool() {
    this.addbuffer = true;
    this.listbuffer = false;
  }
  listschool() {
    this.listbuffer = true;
    this.addbuffer = false;
  }
  deleteSchool(school, index) {
    console.log(school.schoolname);
    this.schoolservice.dropSchool(school.schoolname).subscribe(Response => {
      console.log(Response);
      this.Schools.splice(index, 1);
    });
  }
}
