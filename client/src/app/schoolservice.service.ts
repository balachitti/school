import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SchoolserviceService {
  headers = new HttpHeaders().set('Content-Type', 'application/json');

  constructor(private http: HttpClient) {}

  getSchool() {
    return this.http.get(environment.apiBaseUrl + '/');
  }
  createSchool(schoolName) {
    return this.http.post(environment.apiBaseUrl + `/${schoolName}`, {
      headers: this.headers
    });
  }
  createRegister(schoolName) {
    return this.http.post(
      environment.apiBaseUrl + `/${schoolName}/register`,
      { headers: this.headers }
    );
  }
  addStudent(schoolName, data) {
    return this.http.post(
      environment.apiBaseUrl + `/${schoolName}/student`,
      data,
      { headers: this.headers }
    );
  }
  viewStudentsData(schoolName) {
    return this.http.get(
      environment.apiBaseUrl + `/${schoolName}/student`,
      { headers: this.headers }
    );
  }
  viewBackupData(schoolName) {
    return this.http.get(
      environment.apiBaseUrl + `/${schoolName}/backup`,
      { headers: this.headers }
    );
  }
  viewStudent(schoolName, id) {
    return this.http.get(
      environment.apiBaseUrl + `/${schoolName}/student/${id}`,
      { headers: this.headers }
    );
  }
  updateStudentData(schoolName, id, data) {
    return this.http.put(
      environment.apiBaseUrl + `/${schoolName}/student/${id}`,
      data,
      { headers: this.headers }
    );
  }
  deleteStudentData(schoolName, id) {
    return this.http.delete(
      environment.apiBaseUrl + `/${schoolName}/student/${id}`,
      { headers: this.headers }
    );
  }
  backupStudentdata(schoolName, id) {
    return this.http.post(
      environment.apiBaseUrl + `/${schoolName}/backup/${id}`,
      { headers: this.headers }
    );
  }
  backupSchooldata(schoolName) {
    return this.http.post(environment.apiBaseUrl + `/${schoolName}/backup`, {
      headers: this.headers
    });
  }
  dropSchool(schoolName) {
    return this.http.delete(
      environment.apiBaseUrl + `/${schoolName}`
    );
  }
}
