// require(`../config/config`)
client = require(`../dbconnect`)

createSchool = (request, response) => {
    const name = request.params.schoolname  
    //console.log(name)
    //console.log(typeof(name))
    const createsqlschool = `CREATE SCHEMA IF NOT EXISTS ${name};`
    client.query( createsqlschool, (error, results) => {
      if (error) {
        throw error
      }
      response.status(200).json(`schema created sucessfully`)
      console.log(`schema created sucessfully`)
    })
  }
createRegister = (request,response) =>{
    // const tablename = request.params.tablename
    const name = request.params.schoolname
    //console.log(name)
    const createsqlreg = `CREATE TABLE IF NOT EXISTS ${name}.studentdetails(student_id serial PRIMARY KEY, student_name varchar(30),location varchar(20), languages text);`
      client.query(createsqlreg,(error,results)=>{
      if (error) {
        response.status(500).json('error in creating table')
        throw error
      }
      response.status(200).json(`table created sucessfully`)
      console.log(`table created sucessfully`)
    })
  }

displaySchool = (request,response) =>{
  console.log("list")
    const display = `SELECT s.nspname AS schoolname from pg_catalog.pg_namespace s where nspname not in ('information_schema', 'pg_catalog','public')
 and nspname not like 'pg_toast%' and nspname not like 'pg_temp_%';`
client.query(display,(error,results) =>{
  if(error) { 
    response.status(500).json('error in displaying data')
    throw error
    
    }
    response.status(200).json(results.rows)
})
}

 dropschool=(request,response) =>{
   console.log("drop school")
   const name = request.params.schoolname
   const drop = `DROP SCHEMA ${name} CASCADE`
   client.query(drop,(error,results)=>{
     console.log("deleting ur school")
     if(error){
       response.status(500).json(`error in deleting school`)
       throw error
     }
     response.status(200).json(`The ${name} school sucessfully dropped`)
   })
 }
 backup=(request,Response) => {
   console.log(`preparing for backup the table`)
   const name = request.params.schoolname
   const backupdata =`INSERT INTO public.backup SELECT * , '${name}' FROM ${name}.studentdetails;`
   client.query(backupdata,(error,results)=>{
    console.log("backing up")
    if(error){
     Response.status(500).json(`error can't backup table`)
      throw error
    }

    Response.status(200).json("backup completed sucessfully")
  })

 }

displayBackupdata  = (request,response) =>{
  console.log("student data")
  const displaybackup = `SELECT * FROM public.backup`
    client.query(displaybackup,(error,results) =>{
      if(error){
        response.status(500).json('error in displaying table')
        throw error
        
      }
      response.status(200).json(results.rows)
    })

}

module.exports = {
   createSchool,
   createRegister,
   displaySchool,
   displayBackupdata,
   dropschool,
   backup,
  }
