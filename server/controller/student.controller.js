client = require(`../dbconnect`)
addStudent = (request,response) =>{
    const name = response.locals.schoolname
     //const student_id = request.body.id
     const student_name = request.body.student_name
     const location = request.body.location
     const languages = request.body.languages
     console.log(languages);
    const insertstudent =  `INSERT INTO ${name}.studentdetails ( student_name , location , languages )  VALUES ('${student_name}','${location}','${languages}'); `
    client.query(insertstudent,(error,results) => {
      if (error){
        response.status(500).json('error in inserting table')
        throw error
      }
      response.status(200).json(`sucessfully inserted`)
    })
 }
 displayStudentdata = (request,response) =>{
   console.log("student data")
    const name = response.locals.schoolname
    console.log(name,"hi")
      const displaystudent = `SELECT * FROM ${name}.studentdetails`
      client.query(displaystudent,(error,results) =>{
        if(error){
          response.status(500).json('error in displaying table')
          throw error
          
        }
        response.status(200).json(results.rows)
      })
  
  }
  displayStudent = (request,response) =>{
    console.log("student data id")
    const name = response.locals.schoolname
    const id = request.params.id
      const displaystudent = `SELECT * FROM ${name}.studentdetails WHERE student_id = ${id}`
      client.query(displaystudent,(error,results) =>{
        if(error){
          response.status(500).json('error in creating table')
          throw error
          
        }
        response.status(200).json(results.rows)
      })
  
  }

   updateStudentdata = (request,response) =>{
        console.log("update") 
        const name = response.locals.schoolname
        const id = request.params.id
        const student_name = request.body.student_name
        const location = request.body.location
        const languages = request.body.languages
        console.log(student_name)
        const updatestudentdata =`UPDATE ${name}.studentdetails SET student_name ='${student_name}',location ='${location}',languages = '${languages}'  WHERE student_id = ${id};`
        client.query(updatestudentdata,(error,results) => {
          if(error) { 
            response.status(500).json('error in updating data')
            throw error
           }
           response.status(200).json("updated sucessfully")
        })
   }
   
   deleteStudent=(request,response) =>{
    console.log("delete") 
    const name = response.locals.schoolname
    const id = request.params.id
    const deletesqlstudentdata =`DELETE FROM ${name}.studentdetails WHERE student_id =${id};`
     client.query(deletesqlstudentdata,(error,results)=>{
       console.log(deletesqlstudentdata)
       if(error){
        response.status(500).json('error in deleting table')
         throw error
       }
  
       response.status(200).json("deleted sucessfully")
     })
   }
   backupStudent=(request,response) => {
    console.log(`preparing for backup`)
    const name = response.locals.schoolname
    const id = request.params.id
    const backupdata =`INSERT INTO public.backup SELECT * , '${name}' FROM ${name}.studentdetails WHERE student_id =${id};`
    client.query(backupdata,(error,results)=>{
     console.log("backing up")
     if(error){
      response.status(500).json(`error can't backup table`)
       throw error
     }
  
     response.status(200).json("backup completed sucessfully")
    
   })
  
  }
  module.exports = {
   
    addStudent,
    displayStudentdata,
    displayStudent,
    updateStudentdata,
    deleteStudent,
    backupStudent
    
   }
 