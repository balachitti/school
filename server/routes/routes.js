const express = require('express')
const router = express.Router()
const student = require(`../controller/student.controller`)
const school = require(`../controller/school.controller`)


// router.use('/:schoolname',(req,res,next)=>{
    
    
//         // res.locals.schoolname = req.params.schoolname
//         // res.locals.id  = req.params.id
//         console.log(req.params.schoolname)
//     this.schoolname = req.params.schoolname
//     console.log("hi")
//     next()
// })

router.get('/',school.displaySchool)
router.post('/:schoolname',school.createSchool)
router.post('/:schoolname/register',school.createRegister)
router.delete('/:schoolname',school.dropschool)
router.get('/:schoolname/backup',school.displayBackupdata)
router.post('/:schoolname/backup',school.backup)

router.post('/:schoolname/backup/:id',student.backupStudent)
router.post('/:schoolname/student',student.addStudent)
router.get('/:schoolname/student',student.displayStudentdata)
router.get('/:schoolname/student/:id',student.displayStudent)
router.put('/:schoolname/student/:id',student.updateStudentdata)
router.delete('/:schoolname/student/:id',student.deleteStudent)

module.exports = router